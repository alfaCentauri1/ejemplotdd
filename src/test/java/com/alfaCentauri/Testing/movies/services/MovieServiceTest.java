package com.alfaCentauri.Testing.movies.services;

import com.alfaCentauri.Testing.movies.data.IMovieRepository;
import com.alfaCentauri.Testing.movies.model.Genere;
import com.alfaCentauri.Testing.movies.model.Movie;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class MovieServiceTest {

    private MovieService movieService;

    @Before
    public void setUp() throws Exception {
        IMovieRepository movieRepository = Mockito.mock(IMovieRepository.class);
        Mockito.when(movieRepository.findAll()).thenReturn(
                Arrays.asList(
                        new Movie(1, "Dark Knight", 152, Genere.ACTION, "Chris Nolan"),
                        new Movie(2, "Memento", 113, Genere.THRILLER, "Director 2"),
                        new Movie(3, "There's Something About Mary", 119, Genere.COMEDY, "Director 3"),
                        new Movie(4, "Super 8", 112, Genere.THRILLER, "Spillber"),
                        new Movie(5, "Scream", 111, Genere.HORROR, "Director 3"),
                        new Movie(6, "Home Alone", 103, Genere.COMEDY, "Director 2"),
                        new Movie(7, "Matrix", 136, Genere.ACTION, "Wachoski"),
                        new Movie(8, "Matrix Recargado", 136, Genere.ACTION, "Wachoski"),
                        new Movie(9, "Matrix Revoluciones", 136, Genere.ACTION, "Wachoski"),
                        new Movie(10, "Superman", 120, Genere.ACTION, "Director 1"),
                        new Movie(11, "Superman II", 120, Genere.ACTION, "Director 1"),
                        new Movie(12, "Superman III", 120, Genere.ACTION, "Director 1")

                )
        );
        movieService = new MovieService(movieRepository);
    }

    @Test
    public void return_movies_by_genre() {

        Collection<Movie> movies = movieService.findMoviesByGenre(Genere.COMEDY);
        assertThat(getMovieIds(movies), CoreMatchers.is(Arrays.asList(3, 6)) );
    }

    @Test
    public void return_movies_by_length() {

        Collection<Movie> movies = movieService.findMoviesByLength(119);
        assertThat(getMovieIds(movies), CoreMatchers.is(Arrays.asList(2, 3, 4, 5, 6)) );
    }

    private List<Integer> getMovieIds(Collection<Movie> movies) {
        return movies.stream().map(Movie::getId).collect(Collectors.toList());
    }

    @Test
    public void return_movies_by_name(){
        Collection<Movie> movies = movieService.findMovieByName("Super");
        assertThat(getMovieIds(movies), CoreMatchers.is(Arrays.asList(4, 10, 11, 12)) );
    }

    @Test
    public void return_movies_by_director(){
        Collection<Movie> movies = movieService.findMovieByDirector("Spillber");
        assertThat(getMovieIds(movies), CoreMatchers.is(Arrays.asList(4)) );
    }

    @Test
    public void return_movies_by_times_and_genere(){
        String name = null; // no queremos buscar por nombre
        Integer minutes = 150; // 2h 30m
        Genere genre = Genere.ACTION;
        String director = null;
        Movie template = new Movie(name, minutes, genre, director);
        Collection<Movie> movies = movieService.findMoviesByTemplate(template);
        assertThat(getMovieIds(movies), CoreMatchers.is(Arrays.asList(7,8,9,10,11,12)) );
    }
}