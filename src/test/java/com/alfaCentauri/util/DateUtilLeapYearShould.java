package com.alfaCentauri.util;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertThat;

public class DateUtilLeapYearShould {
    @Test
    public void return_true_when_year_is_divisible_by_400() {
        assertThat(DateUtil.isLeapYear(1600), CoreMatchers.is(true));
        assertThat(DateUtil.isLeapYear(2000), CoreMatchers.is(true));
        assertThat(DateUtil.isLeapYear(2400), CoreMatchers.is(true));
    }
}