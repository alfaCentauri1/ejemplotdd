package com.alfaCentauri.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class RomanNumbersTest {

    @Test
    public void arabicToRoman() {
        String result = RomanNumbers.arabicToRoman(1);
        assertEquals("I", result);
    }

    @Test
    public void arabicToRoman_2() {
        String result = RomanNumbers.arabicToRoman(2);
        assertEquals("II", result);
    }

    @Test
    public void arabicToRoman_3() {
        String result = RomanNumbers.arabicToRoman(3);
        assertEquals("III", result);
    }

    @Test
    public void arabicToRoman_4() {
        String result = RomanNumbers.arabicToRoman(4);
        assertEquals("IV", result);
    }

    @Test
    public void arabicToRoman_5() {
        String result = RomanNumbers.arabicToRoman(5);
        assertEquals("V", result);
    }

    @Test
    public void arabicToRoman_6() {
        String result = RomanNumbers.arabicToRoman(6);
        assertEquals("VI", result);
    }

    @Test
    public void arabicToRoman_7() {
        String result = RomanNumbers.arabicToRoman(7);
        assertEquals("VII", result);
    }
}