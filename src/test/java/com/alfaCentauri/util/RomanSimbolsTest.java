package com.alfaCentauri.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class RomanSimbolsTest {

    @Test
    public void arabicRoman_7() {
        String result = RomanSimbols.arabicRoman(7);
        assertEquals("VII", result);
    }

    @Test
    public void arabicRoman_3() {
        String result = RomanSimbols.arabicRoman(3);
        assertEquals("III", result);
    }

    @Test
    public void arabicRoman_10() {
        String result = RomanSimbols.arabicRoman(10);
        assertEquals("X", result);
    }

    @Test
    public void arabicRoman_11() {
        String result = RomanSimbols.arabicRoman(11);
        assertEquals("XI", result);
    }

    @Test
    public void arabicRoman_16() {
        String result = RomanSimbols.arabicRoman(16);
        assertEquals("XVI", result);
    }

    @Test
    public void arabicRoman_50() {
        String result = RomanSimbols.arabicRoman(50);
        assertEquals("L", result);
    }

    @Test
    public void arabicRoman_51() {
        String result = RomanSimbols.arabicRoman(51);
        assertEquals("LI", result);
    }

    @Test
    public void arabicRoman_56() {
        String result = RomanSimbols.arabicRoman(56);
        assertEquals("LVI", result);
    }

    @Test
    public void arabicRoman_99() {
        String result = RomanSimbols.arabicRoman(99);
        assertEquals("XCIX", result);
    }

    @Test
    public void arabicRoman_299() {
        String result = RomanSimbols.arabicRoman(299);
        assertEquals("CCXCIX", result);
    }
}