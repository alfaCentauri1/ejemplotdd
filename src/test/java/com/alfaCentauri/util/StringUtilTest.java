package com.alfaCentauri.util;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilTest {
    @Test
    public void repeat_string_once() {
        Assert.assertEquals("hola", StringUtil.repeat("hola", 1));
    }

    @Test
    public void repeat_string_multiple_times() {
        Assert.assertEquals("holaholahola", StringUtil.repeat("hola", 3));
    }

    @Test
    public void repeat_string_zero_times() {
        Assert.assertEquals("", StringUtil.repeat("hola", 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void repeat_string_negative_times() {
        StringUtil.repeat("hola", -1);
    }

    @Test
    public void isEmptyWithNull(){
        boolean result = StringUtil.isEmpty(null);
        Assert.assertTrue(result);
    }

    @Test
    public void isEmptyWithEmpty(){
        boolean result = StringUtil.isEmpty("");
        Assert.assertTrue(result);
    }

    @Test
    public void isEmptyWithSpaceBlank(){
        boolean result = StringUtil.isEmpty("    ");
        Assert.assertTrue(result);
    }
}