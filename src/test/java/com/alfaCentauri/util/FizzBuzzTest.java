package com.alfaCentauri.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {
    @Test
    public void testFizzBuzz() {
        String result = FizzBuzz.fizzBuzz(3);
        assertEquals("Fizz", result);
    }

    @Test
    public void testFizzBuzzWithSix() {
        String result = FizzBuzz.fizzBuzz(6);
        assertEquals("Fizz", result);
    }

    @Test
    public void testFizzBuzzWithFive() {
        String result = FizzBuzz.fizzBuzz(5);
        assertEquals("Buzz", result);
    }

    @Test
    public void testFizzBuzzWithTen() {
        String result = FizzBuzz.fizzBuzz(10);
        assertEquals("Buzz", result);
    }

    @Test
    public void testFizzBuzzWith_15() {
        String result = FizzBuzz.fizzBuzz(15);
        assertEquals("FizzBuzz", result);
    }

    @Test
    public void testFizzBuzzWith_30() {
        String result = FizzBuzz.fizzBuzz(30);
        assertEquals("FizzBuzz", result);
    }

    @Test
    public void testFizzBuzzWith_2() {
        String result = FizzBuzz.fizzBuzz(2);
        assertEquals("2", result);
    }

    @Test
    public void testFizzBuzzWith_16() {
        String result = FizzBuzz.fizzBuzz(16);
        assertEquals("16", result);
    }
}