package com.alfaCentauri.Testing.payments;

public interface PaymentGateway {

    PaymentResponse requestPayment(PaymentRequest request);
}
