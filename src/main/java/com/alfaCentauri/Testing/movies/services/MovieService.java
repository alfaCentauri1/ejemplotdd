package com.alfaCentauri.Testing.movies.services;

import com.alfaCentauri.Testing.movies.data.IMovieRepository;
import com.alfaCentauri.Testing.movies.model.Genere;
import com.alfaCentauri.Testing.movies.model.Movie;

import java.util.Collection;
import java.util.stream.Collectors;

public class MovieService {
    private IMovieRepository movieRepository;

    public MovieService(IMovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public Collection<Movie> findMoviesByGenre(Genere genre) {

        return movieRepository.findAll().stream()
                .filter(movie -> movie.getGenre() == genre).collect(Collectors.toList());
    }

    public Collection<Movie> findMoviesByLength(int length) {
        return movieRepository.findAll().stream()
                .filter(movie -> movie.getMinutes() <= length).collect(Collectors.toList());
    }

    /**
     * @param name Type String.
     **
    public Collection<Movie> findMovieByName(String name) {
        return movieRepository.findByName(name);
    }*/

    /**
     * @param name Type String.
     **/
    public Collection<Movie> findMovieByName(String name) {
        return movieRepository.findAll().stream()
                .filter(movie -> movie.getName().contains(name)).collect(Collectors.toList());
    }

    /**
     * @param name Type String.
     **/
    public Collection<Movie> findMovieByDirector(String name) {
        return movieRepository.findAll().stream()
                .filter(movie -> movie.getDirector().contains(name)).collect(Collectors.toList());
    }

    /**
     * @param template Type Movi.
     * @return Return a collection with result.
     **/
    public Collection<Movie> findMoviesByTemplate(Movie template) {
        return movieRepository.findAll().stream()
                .filter(movie -> ( movie.getMinutes() <= template.getMinutes() ) && ( movie.getGenre().equals(template.getGenre()) ) )
                .collect(Collectors.toList());
    }

}
