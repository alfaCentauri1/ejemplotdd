package com.alfaCentauri.Testing.movies.model;

public enum Genere {
    ACTION, COMEDY, DRAMA, HORROR, THRILLER
}
