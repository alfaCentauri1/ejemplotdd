package com.alfaCentauri.Testing.movies.data;

import com.alfaCentauri.Testing.movies.model.Movie;

import java.util.Collection;

public interface IMovieRepository {
    Movie findById(long id);
    Collection<Movie> findAll();
    void saveOrUpdate(Movie movie);

    Collection<Movie> findByName(String name);
}
