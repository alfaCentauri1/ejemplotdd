package com.alfaCentauri.util;

public class StringUtil {
    /**
     *
     * @param str Type String.
     * @param times Type int.
     * @return Return a string repeat.
     */
    public static String repeat(String str, int times) {

        if (times < 0) {
            throw new IllegalArgumentException("negative times not allowed");
        }

        String result = "";

        for (int i = 0; i < times; i++) {
            result += str;
        }

        return result;
    }

    /**
     *
     * @param str Type String.
     * @return Return a boolean.
     */
    public static boolean isEmpty(String str) {
        boolean result = false;
        str = str != null ? str.trim():null;
        if ( str == null || str.equals("") )
            result = true;

        return result;
    }
}
