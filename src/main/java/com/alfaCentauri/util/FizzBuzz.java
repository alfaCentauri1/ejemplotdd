package com.alfaCentauri.util;

public class FizzBuzz {

    /**
     * @param n Type int.
     * @return Return a string.
     **/
    public static String fizzBuzz(int n) {
        String result = "";
        if (n % 3 == 0) {
            result = "Fizz";
        }
        if ( n % 5 == 0 ) {
            result += "Buzz";
        }
        if ( (n % 3 != 0) && ( n % 5 != 0 ) ) {
            result = String.valueOf(n);
        }
        return result;
    }
}
