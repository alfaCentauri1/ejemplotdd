package com.alfaCentauri.util;

import java.util.Arrays;
import java.util.List;

public enum RomanSimbols {

    M(1000),CM(900),D(500),CD(400),C(100),XC(90),L(50),XL(40),
    X(10),IX(9),V(5),IV(4),I(1) ;

    private int valor;

    RomanSimbols(int valor){
        this.valor = valor;
    }

    public int getValor(){
        return this.valor;
    }

    public static List<RomanSimbols> getRomanSimbols(){
        return Arrays.asList(RomanSimbols.values());
    }

    public static String arabicRoman(int n){
        StringBuilder collator = new StringBuilder();
        List <RomanSimbols> romanNumbers = RomanSimbols.getRomanSimbols();
        //Validación
        if (n > 0 && n < 4000){
            int i = 0;
            while (n > 0 && i < romanNumbers.size()){
                RomanSimbols currentRoman = romanNumbers.get(i);
                if ( n >= currentRoman.getValor() ){
                    collator.append(currentRoman);
                    n-=currentRoman.getValor();
                }
                else {
                    i++;
                }
            }
            String roman = collator.toString();
            return roman;
        }
        else {
            throw new IllegalArgumentException( n + "no esta en el rango de 0 a 4000.");
        }
    }
}
