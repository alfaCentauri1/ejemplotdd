package com.alfaCentauri.util;

public class DateUtil {
    /**
     * @param year Type integer.
     * @return Return a boolean.
     **/
    public static boolean isLeapYear(int year){
        return ( (year % 4 == 0 && year % 100 != 0) || ( year % 400 == 0 ) );
    }
}
