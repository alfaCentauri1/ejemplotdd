# EjemploTDD

Ejemplo de testing con JUnit 4. También se muestra como probar una exception con un test de funcionalidad.

## Requisitos

* JDK 11 ó superior.
* MySQL 5.5 ó superior.
* Maven.

***

## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ejecute desde la consola; en la raiz del proyecto, java Ejecutable.jar

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
* Developer
